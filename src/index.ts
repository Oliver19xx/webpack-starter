import * as _ from 'lodash';
import "./style.scss";

  function title() {
    const element = document.createElement('b');
    element.innerText = 'Webpack-Setup mit folgenden Features:'
    return element;
  }
  function list() {
    const ul = document.createElement('ul');
    const dataList = ['SCSS &rarr; CSS','TS &rarr; JS'];
    dataList.forEach(e=>{
        const li = document.createElement('li');
        li.innerHTML = e;
        ul.appendChild(li);
    });

    return ul;
  }

  document.body.appendChild(title());
  document.body.appendChild(list());
